<?php

namespace Drupal\commerce_logistics;

use Drupal\Component\Plugin\PluginBase;

/**
 * @see \Drupal\commerce_logistics\Annotation\LogisticsQuery
 * @see \Drupal\commerce_logistics\LogisticsQueryInterface
 */
abstract class LogisticsQueryBase extends PluginBase implements LogisticsQueryInterface {

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function type() {
    return (string) $this->pluginDefinition['type'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function shipping_info_data(array $param);

}

<?php

namespace Drupal\commerce_logistics;

interface LogisticsQueryInterface {

  /**
   * @return string
   */
  public function description();

  /**
   * @return string
   */
  public function type();

  /**
   * @param array $param
   *
   * @return array
   */
  public function shipping_info_data(array $param);

}

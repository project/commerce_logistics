<?php

namespace Drupal\commerce_logistics\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the default form handler for the Shipment Information entity.
 */
class ShipmentInfoForm extends EntityForm {

  public function prepareEntity() {
    parent::prepareEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $label = $this->entity->label();
    $status = $this->entity->get('status');
    $shipment_id = $this->entity->get('shipment_id');
    if ($this->entity->isNew()) {
      $shipment = \Drupal::routeMatch()->getParameter('commerce_shipment');
      $mail_number = $shipment->getTrackingCode();
      $shipping_method_id = $shipment->getShippingMethod()->id();
      $config = \Drupal::config('commerce_logistics.shipping_info_query');
      $company_code = $config->get("code$shipping_method_id");
      $label = strtolower($company_code . $mail_number);
      $status = 0;
      $shipment_id = $shipment->id();
    }
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Name"),
      '#description' => $this->t("The shipment information title."),
      '#default_value' => $label,
    ];
    $form['id'] = [
      '#type' => "machine_name",
      '#title' => $this->t("Name"),
      '#description' => $this->t("A unique machine-readable name for this entity. It must only contain lowercase letters, numbers, and underscores."),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['Drupal\commerce_logistics\Entity\ShipmentInfo', 'load'],
        'source' => ['label'],
      ],
    ];
    $form['created'] = [
      '#type' => "textfield",
      '#title' => $this->t("Created"),
      '#description' => $this->t("Shipment information created time."),
      '#default_value' => $this->entity->get('created'),
    ];
    $form['changed'] = [
      '#type' => "textfield",
      '#title' => $this->t("Changed"),
      '#description' => $this->t("Shipment information updated time."),
      '#default_value' => $this->entity->get('changed'),
    ];
    $form['shipment_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Shipment ID"),
      '#description' => $this->t("The ID of the associated shipment."),
      '#default_value' => $shipment_id,
    ];
    $form['shipment_detail'] = [
      '#type' => 'text_format',
      '#title' => $this->t("Detail"),
      '#format' => 'full_html',
      '#description' => $this->t("Explanatory text on the shipping situation."),
      '#default_value' => $this->entity->get('shipment_detail'),
    ];
    $form['status'] = [
      '#type' => "number",
      '#title' => $this->t("status"),
      '#description' => $this->t("Shipping status.(0:Draft, 1:Ready, 2:On the way, 3:Sign for, 4:Issue)"),
      '#default_value' => $status,
      '#min' => 0,
      '#max' => 4,
      '#step' => 1,
    ];
    $form['remarks'] = [
      '#type' => 'text_format',
      '#title' => $this->t("Remarks"),
      '#format' => 'full_html',
      '#description' => $this->t("Remarks on shipment information.For example, the cause of the issue."),
      '#default_value' => $this->entity->get('remarks'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $saved;
  }

}

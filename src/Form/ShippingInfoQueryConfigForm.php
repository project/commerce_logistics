<?php

/**
 * @file
 * Contains \Drupal\commerce_logistics\Form\AggregationLogisticsQueryConfigForm.
 */

namespace Drupal\commerce_logistics\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default config form for Shipment Info Query.
 * Matching configuration of kdniao interface and logistics method with
 * logistics code 快递鸟接口和物流方法与物流编码配对配置
 */
class ShippingInfoQueryConfigForm extends ConfigFormBase {

  /**
   * The logistics company storage.
   *
   * @var \Drupal\commerce_shipping\ShippingMethodStorage
   */
  protected $shippingMethodStorage;

  /**
   * Constructs a new ShippingMethodCodeForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->shippingMethodStorage = $entity_type_manager->getStorage('commerce_shipping_method');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'shipping_info_query_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_logistics.shipping_info_query'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_logistics.shipping_info_query');
    $form['kdniao'] = [
      '#type' => 'fieldset',
      '#title' => t("kdniao platform settings"),
    ];
    $form['kdniao']['callback_url'] = [
      '#type' => 'textfield',
      '#title' => t('Callback url'),
      '#description' => t('Callback url'),
      '#default_value' => $config->get('callback_url'),
      '#required' => TRUE,
    ];
    $form['kdniao']['e_business_id'] = [
      '#type' => 'textfield',
      '#title' => t('EBusinessID'),
      '#description' => t('EBusinessID'),
      '#default_value' => $config->get('e_business_id'),
      '#required' => TRUE,
    ];
    $form['kdniao']['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('ApiKey'),
      '#description' => t('ApiKey'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];
    $form['kdniao']['interval_time'] = [
      '#type' => 'number',
      '#title' => t('Interval time'),
      '#description' => t('Interval between queries(unit:seconds)'),
      '#default_value' => $config->get('interval_time') ?? 0,
      '#min' => 0,
      '#step' => 60,
      '#required' => TRUE,
    ];
    $form['shipping_methods_code'] = [
      '#type' => 'fieldset',
      '#title' => t("Shipping methods code"),
      '#description' => t('Compiled by the National Post Office of China, provided by the official website of the National Post Office.'),
    ];
    $shipping_methods = $this->shippingMethodStorage->loadMultiple();
    foreach ($shipping_methods as $shipping_method) {
      $shipping_method_name = $shipping_method->getName();
      $shipping_method_id = $shipping_method->id();
      $form['shipping_methods_code'][$shipping_method_id] = [
        '#type' => 'fieldset',
        '#title' => t("Configure %name shipping method's Code", [
          '%name' => $shipping_method_name,
        ]),
      ];
      $form['shipping_methods_code'][$shipping_method_id]['name' . $shipping_method_id] = [
        '#type' => 'markup',
        '#markup' => "$shipping_method_name",
      ];
      $form['shipping_methods_code'][$shipping_method_id]['code' . $shipping_method_id] = [
        '#type' => 'textfield',
        '#title' => t('Shipping method code.'),
        '#default_value' => $config->get("code{$shipping_method_id}"),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Runs cron and reloads the page.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $shipping_method_code_config = $this->config('commerce_logistics.shipping_info_query');
    $shipping_method_code_config->set('callback_url', $form_state->getValue('callback_url'))
      ->set('e_business_id', $form_state->getValue('e_business_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('interval_time', $form_state->getValue('interval_time'));
    $shipping_methods = $this->shippingMethodStorage->loadMultiple();
    foreach ($shipping_methods as $shipping_method) {
      $shipping_method_id = $shipping_method->id();
      $shipping_method_code_config->set("code{$shipping_method_id}", $form_state->getValue("code{$shipping_method_id}"));
    }
    $shipping_method_code_config->save();
    parent::submitForm($form, $form_state);
  }

}

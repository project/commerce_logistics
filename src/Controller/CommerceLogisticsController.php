<?php
/**
 */

namespace Drupal\commerce_logistics\Controller;

use Drupal\commerce_logistics\LogisticsQueryPluginManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CommerceLogisticsController extends ControllerBase {

  /**
   * @var \Drupal\commerce_logistics\LogisticsQueryPluginManager
   */
  protected $logisticsQueryManager;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_logistics\LogisticsQueryPluginManager $logistics_query_manager
   *
   */
  public function __construct(LogisticsQueryPluginManager $logistics_query_manager) {
    $this->logisticsQueryManager = $logistics_query_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @see container
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.logistics_query'));
  }

}

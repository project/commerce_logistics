<?php

namespace Drupal\commerce_logistics\Entity;

use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Provides the shipment information entity.
 *
 * @ConfigEntityType(
 *   id = "shipment_info",
 *   label = @Translation("Shipment information"),
 *   label_collection = @Translation("Shipment informations"),
 *   label_singular = @Translation("Shipment information"),
 *   label_plural = @Translation("Shipment informations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Shipment information",
 *     plural = "@count Shipment informations",
 *   ),
 *   handlers = {
 *     "access" =
 *   "Drupal\commerce_logistics\Entity\Handler\ShipmentInfoAccess",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\commerce_logistics\Form\ShipmentInfoForm",
 *       "add" = "Drupal\commerce_logistics\Form\ShipmentInfoAddForm",
 *       "edit" = "Drupal\commerce_logistics\Form\ShipmentInfoEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "storage" =
 *   "Drupal\commerce_logistics\Entity\Handler\ShipmentInfoStorage",
 *     "list_builder" =
 *   "Drupal\commerce_logistics\Entity\Handler\ShipmentInfoListBuilder",
 *   },
 *   admin_permission = "administer shipment_info entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "created",
 *     "changed",
 *     "shipment_id",
 *     "shipment_detail",
 *     "status",
 *     "remarks",
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/shipment-info/add",
 *     "collection" = "/admin/commerce/shipment-info",
 *     "edit-form" = "/admin/commerce/shipment-info/{shipment_info}/edit",
 *     "delete-form" = "/admin/commerce/shipment-info/{shipment_info}/delete",
 *   },
 * )
 */
class ShipmentInfo extends ConfigEntityBase implements ShipmentInfoInterface {

  /**
   * Machine name.
   *
   * @var string
   */
  protected $id = '';

  /**
   * Name.
   *
   * @var string
   */
  protected $label = '';

  /**
   * Created.
   *
   * @var string
   */
  protected $created = '';

  /**
   * Changed.
   *
   * @var string
   */
  protected $changed = '';

  /**
   * Shipment_id.
   *
   * @var integer
   */
  protected $shipment_id = '';

  /**
   * Shipment Detail.
   *
   * @var string
   */
  protected $shipment_detail = '';

  /**
   * Status.
   *
   * @var integer
   */
  protected $status = '';

  /**
   * Remarks.
   *
   * @var string
   */
  protected $remarks = '';

  public function applyShipped($state, $shipment) {
    switch ($state) {
      case "draft":
        $shipment->getState()->applyTransitionById('finalize');
        $shipment->getState()->applyTransitionById('ship');
        $shipment->save();
        break;
      case "ready":
        $shipment->getState()->applyTransitionById('ship');
        $shipment->save();
        break;
    }
    return $shipment;
  }

  public function applyCompleted($state, $shipment) {
    $shipment = $this->applyShipped($state, $shipment);
    $state = $shipment->getState()->getId();
    if ($state == "shipped") {
      $shipment->getState()->applyTransitionById('complete');
      $shipment->save();
    }
    return $shipment;
  }

  public function applyIssue($state, $shipment) {
    $shipment = $this->applyShipped($state, $shipment);
    $state = $shipment->getState()->getId();
    if ($state == "shipped") {
      $shipment->getState()->applyTransitionById('issue');
      $shipment->save();
    }
    return $shipment;
  }

  public function getShipment() {
    $shipment_id = $this->get('shipment_id');
    return Shipment::load($shipment_id);
  }

  public function getShipmentWorkflowId() {
    $shipment = $this->getShipment();
    return $shipment->getState()->getId();
  }

}

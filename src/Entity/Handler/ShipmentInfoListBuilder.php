<?php

namespace Drupal\commerce_logistics\Entity\Handler;

use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\ShipmentStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the list builder handler for the Shipment Information entity.
 */
class ShipmentInfoListBuilder extends EntityListBuilder {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The ShipmentStorage.
   *
   * @var \Drupal\commerce_shipping\ShipmentStorageInterface
   */
  protected $shipmentStorage;

  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, RouteMatchInterface $route_match, ShipmentStorageInterface $shipmentStorage) {
    parent::__construct($entity_type, $storage);
    $this->routeMatch = $route_match;
    $this->shipmentStorage = $shipmentStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')->getStorage('commerce_shipment')
    );
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $order = $this->routeMatch->getParameter('commerce_order');
    $shipments = $this->shipmentStorage->loadMultipleByOrder($order);
    $shipment_ids = [];
    foreach ($shipments as $shipment) {
      $shipment_ids[$shipment->id()] = $shipment->id();
    }
    $query = $this->getStorage()->getQuery()
      ->condition('shipment_id', $shipment_ids, 'IN')
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('id'));
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    $result = $query->execute();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['shipment'] = $this->t('Shipment');
    $header['name'] = $this->t('Name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $shipment = Shipment::load($entity->get('shipment_id'));
    $row['shipment'] = $shipment->toLink();
    $row['name'] = $entity->label();
    $status = $entity->get('status');
    switch ($status) {
      case 0:
        $row['status'] = $this->t("Draft");
        break;
      case 1:
        $row['status'] = $this->t("Ready");
        break;
      case 2:
        $row['status'] = $this->t("On the way");
        break;
      case 3:
        $row['status'] = $this->t("Sign for");
        break;
      case 4:
        $row['status'] = $this->t("Issue");
        break;
      default:
        break;
    }
    return $row + parent::buildRow($entity);
  }

}

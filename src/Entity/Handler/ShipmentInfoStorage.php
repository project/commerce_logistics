<?php

namespace Drupal\commerce_logistics\Entity\Handler;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Provides the storage handler for the Shipment Information entity.
 */
class ShipmentInfoStorage extends ConfigEntityStorage {

}

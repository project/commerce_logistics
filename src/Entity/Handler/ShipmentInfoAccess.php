<?php

namespace Drupal\commerce_logistics\Entity\Handler;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Provides the access handler for the Shipment Information entity.
 */
class ShipmentInfoAccess extends EntityAccessControlHandler {

}

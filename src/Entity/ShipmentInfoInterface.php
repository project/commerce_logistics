<?php

namespace Drupal\commerce_logistics\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Shipment Information entities.
 */
interface ShipmentInfoInterface extends ConfigEntityInterface {

  public function applyShipped($state, $shipment);
  public function applyCompleted($state, $shipment);
  public function applyIssue($state, $shipment);
  public function getShipment();
  public function getShipmentWorkflowId();

}

<?php

namespace Drupal\commerce_logistics\Resolver;

use Drupal\commerce_logistics\LogisticsQueryPluginManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 */
class DefaultLogisticsQueryResolver implements LogisticsQueryTypeResolverInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\commerce_logistics\LogisticsQueryPluginManager
   */
  protected $logisticsQueryManager;

  /**
   * The logistics detail storage.
   *
   * @var \Drupal\commerce_shipping\ShipmentStorage
   */
  protected $commerceShipmentStorage;

  /**
   * The shipment information storage.
   *
   * @var \Drupal\commerce_logistics\Entity\Handler\ShipmentInfoStorage
   */
  protected $shipmentInfoStorage;

  /**
   * The logistics company storage.
   *
   * @var \Drupal\commerce_shipping\ShippingMethodStorage
   */
  protected $commerceShippingMethodStorage;

  /**
   * Constructs a new OrderStoreResolver object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\commerce_logistics\LogisticsQueryPluginManager $logistics_query_manager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(RouteMatchInterface $route_match, LogisticsQueryPluginManager $logistics_query_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->logisticsQueryManager = $logistics_query_manager;
    $this->commerceShipmentStorage = $entity_type_manager->getStorage('commerce_shipment');
    $this->shipmentInfoStorage = $entity_type_manager->getStorage('shipment_info');
    $this->commerceShippingMethodStorage = $entity_type_manager->getStorage('commerce_shipping_method');
  }

  /**
   * @see container
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.logistics_query'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderInterface $order) {
    if (!empty($order)) {
      /*========Start:Query the shipment according to the order========*/
      /*========Start:根据订单查询物流====================================*/
      $shipmentStorage = $this->commerceShipmentStorage;
      $shipments = $shipmentStorage->loadMultipleByOrder($order);
      //TODO Consider package splitting later
      $shipment = reset($shipments);
      /*========================================End============================*/

      /*========Start:Query the shipment_info entity according to the shipment========*/
      /*========Start:根据commerce_shipment查询shipment_info============================*/
      $shipment_id = $shipment->id();
      $shipment_infos = $this->shipmentInfoStorage->loadByProperties([
        'shipment_id' => $shipment_id,
      ]);
      $shipment_info = reset($shipment_infos);
      /*========End===================================================================*/

      /*==Start:Calculate the time interval of the latest stored shipment information and the current time==*/
      /*==Start:计算最新的物流信息的更新时间距现在过了多久===========================================================*/
      $newest_node_time = 0;
      if (!empty($newest_logistics_node)) {
        $newest_node_time = $shipment_info->get('changed');
      }
      $now = time();
      $passed_time = $now - $newest_node_time;
      /*========================================End===========================================================*/

      $shipment_state = $shipment->getState()->getId();
      $stop_query_states = ["completed", "canceled", "issue"];
      $interval_time = \Drupal::config('commerce_logistics.shipping_info_query')
        ->get('interval_time');
      /**
       * When the time interval between the latest shipment information stored and the current time exceeds the configured time interval,
       * and the status of the shipment is not one of completed, canceled, or issued, perform an online query on the logistics interface.
       * 当存储的最新物流信息更新时间超过配置的重新查询的间隔时间，并且发货状态不是已完成、已取消或已发出时进行在线查询。
       */
      if ($passed_time > $interval_time || !in_array($shipment_state, $stop_query_states)) {
        //Obtain the logistics company code
        //获取物流方法对应的物流编码
        $shipping_method = $shipment->getShippingMethod();
        $shipping_method_id = $shipping_method->id();
        $shipping_method_code_config = \Drupal::config('commerce_logistics.shipping_info_query');
        $company_code = $shipping_method_code_config->get("code{$shipping_method_id}");

        //Query whether there is a query plug-in for the shipping method according to the shipping method code
        //根据发货方式编码查询发货方式是否有独立的查询插件
        $logistics_query_plugin_definitions = $this->logisticsQueryManager->getDefinitions();
        foreach ($logistics_query_plugin_definitions as $logistics_query_plugin_definition) {
          if ($company_code == $logistics_query_plugin_definition['type']) {
            return $company_code . '_logistics_query';
          }
        }
        //If there is no logistics query plug-in with the specified shipping method code, use the logistics query platform plug-in
        //如果没有指定物流编码的物流查询插件，则使用物流查询平台插件(快递鸟)
        return 'aggregation_logistics_query';
      }
      else {
        //If the configured time interval is not exceeded
        //如果更新时间没有超过配置的重新查询的时间间隔，取缓存/数据库信息
        return 'shipment_info_query';
      }
    }
    return NULL;
  }

}

<?php

namespace Drupal\commerce_logistics\Resolver;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the interface for order type resolvers.
 */
interface LogisticsQueryTypeResolverInterface {

  /**
   * Resolves the logistics query plugin type.
   */
  public function resolve(OrderInterface $order);

}

<?php
/**
 * @file
 * Contains \Drupal\commerce_logistics\Plugin\QueueWorker\ShipmentQueryQueue.
 */

namespace Drupal\commerce_logistics\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes Tasks for Query Shipment Information.
 *
 * @QueueWorker(
 * id = "shipment_query_queue",
 * title = @Translation("Shipment query task worker: shipment info queue"),
 * cron = {"time" = 60}
 * )
 */
class ShipmentQueryQueue extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $shipping_method_id = $data->getShippingMethod()->id();
    $config = \Drupal::config('commerce_logistics.shipping_info_query');
    $company_code = $config->get("code$shipping_method_id");
    $mail_no = $data->getTrackingCode();
    if (!empty($company_code) && !empty($mail_no) && !empty($data->getOrder())) {
      $param = [
        'com' => $company_code,
        'num' => $mail_no,
      ];
      $plugin_id = \Drupal::service('commerce_logistics.default_logistics_query_resolver')
        ->resolve($data->getOrder());
      $nodes_logistics_query = \Drupal::service('plugin.manager.logistics_query')
        ->createInstance($plugin_id);
      $nodes_logistics_query->shipping_info_data($param);
    }
  }

}

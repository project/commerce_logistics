<?php

namespace Drupal\commerce_logistics\Plugin\LogisticsQuery;

use Drupal\commerce_logistics\LogisticsQueryBase;

/**
 * @LogisticsQuery(
 *   id = "shipment_info_query",
 *   description = @Translation("Manually entered logistics information."),
 *   type = "0",
 * )
 */
class ShipmentInfoQuery extends LogisticsQueryBase {

  /**
   *
   * @param array $param
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function shipping_info_data(array $param) {
    $com = $param['com'];
    $mail_no = $param['num'];
    $data = \Drupal::cache('data')->get($mail_no);
    if(!$data){
      $shipping_methods = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method')->loadMultiple();
      $shipping_info_query_config = \Drupal::config('commerce_logistics.shipping_info_query');
      foreach ($shipping_methods as $shipping_method) {
        $shipping_method_id = $shipping_method->id();
        $code = $shipping_info_query_config->get("code{$shipping_method_id}");
        if($code == $com){
          $shipping_method_id = $shipping_method->id();
          $shipments = \Drupal::entityTypeManager()
            ->getStorage('commerce_shipment')
            ->loadByProperties([
              'tracking_code' => $mail_no,
              'shipping_method' => $shipping_method_id,
            ]);
          $shipment = reset($shipments);
          $logistics_detail_nodes = \Drupal::entityTypeManager()
            ->getStorage('shipment_info')
            ->loadByProperties([
              'shipment_id' => $shipment->id(),
            ]);
          $logistics_detail_node = reset($logistics_detail_nodes);
          $data = [
            "context" => $logistics_detail_node->get('node_detail')->value,
            "remarks" => $logistics_detail_node->get('remarks')->value,
          ];
          break;
        }
      }
    }else{
      \Drupal::logger('commerce_logistics')->debug(json_encode($data));
    }
    return $data;
  }

}

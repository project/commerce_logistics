<?php

namespace Drupal\commerce_logistics\Plugin\LogisticsQuery;

use Drupal\commerce_logistics\LogisticsQueryBase;

/**
 * kdniao Logistics Query Plugin
 * 快递鸟物流查询插件
 *
 * @LogisticsQuery(
 *   id = "aggregation_logistics_query",
 *   description = @Translation("Aggregation logistics query."),
 *   type = "1",
 * )
 */
class AggregationLogisticsQuery extends LogisticsQueryBase {

  /**
   *
   * @param array $param
   *
   * @return array|null
   */
  public function shipping_info_data(array $param) {
    $com = $param['com'];
    $num = $param['num'];
    if (empty($com)) {
      \Drupal::messenger()
        ->addMessage(t("Please check whether the logistics method code is set"));
    }
    if (empty($num)) {
      \Drupal::messenger()
        ->addMessage(t("Please check whether the tracking code is set"));
    }

    if (!empty($com) && !empty($num)) {
      return $this->getOrderTracesByJson($com, $num);
    }
    else {
      return NULL;
    }
  }

  public function getOrderTracesByJson($com, $num) {
    //组装应用级参数
    $requestData = "{" .
      "'CustomerName': ''," .
      "'OrderCode': ''," .
      "'ShipperCode': '$com'," .
      "'LogisticCode': '$num'," .
      "}";
    // 组装系统级参数
    $e_business_id = \Drupal::config('commerce_logistics.shipping_info_query')
      ->get('e_business_id');
    $api_key = \Drupal::config('commerce_logistics.shipping_info_query')
      ->get('api_key');
    //Free real-time query interface instruction 1002/in-transit monitoring real-time query interface instruction 8001/map version real-time query interface instruction 8003
    $datas = [
      'EBusinessID' => $e_business_id,
      'RequestType' => '1002',
      'RequestData' => urlencode($requestData),
      'DataType' => '2',
    ];
    $datas['DataSign'] = $this->encrypt($requestData, $api_key);
    //Submit the post request in the form of form. The post request body contains application-level parameters and system-level parameters
    $json_result = $this->sendPost($datas);
    $result = json_decode($json_result, TRUE);
    if (is_array($result) && array_key_exists('Traces', $result)) {
      $data['context'] = "";
      $shipments = \Drupal::entityTypeManager()
        ->getStorage('commerce_shipment')
        ->loadByProperties([
          'tracking_code' => $num,
        ]);
      $newest_query_time = strtotime($result['Traces'][0]['AcceptTime']);
      foreach ($result['Traces'] as $trace) {
        $data['context'] .= "<div class='detail-row'><span class='dot'></span><span class='accept-time'>" . $trace['AcceptTime'] . "</span><span class='accept-station'>" . $trace['AcceptStation'] . "</span></div><br>";
      }
      $data['remarks'] = $result['Reason'];
      if(!empty($shipments)){
        /*===========Start:Store the queried logistics node information in the database===========*/
        //Query the shipment information entity of shipment and obtain the latest updated time
        $shipment = end($shipments);
        $shipment_id = $shipment->id();
        $shipment_info_id = strtolower($com . $num);
        $shipment_infos = \Drupal::entityTypeManager()
          ->getStorage('shipment_info')
          ->loadByProperties([
            'id' => $shipment_info_id,
          ]);
        $shipment_info_newest_time = 0;
        if (!empty($shipment_infos)) {
          $shipment_info = reset($shipment_infos);
          $shipment_info_newest_time = $shipment_info->get('changed');
        }
        else {
          $shipment_info = \Drupal::entityTypeManager()
            ->getStorage('shipment_info')
            ->create([
              'id' => $shipment_info_id,
              'label' => $shipment_info_id,
              'created' => (string) time(),
              'changed' => (string) time(),
              'shipment_id' => $shipment_id,
              'shipment_detail' => $data['context'],
              'status' => $result['State'],
              'remarks' => $data['remarks'],
            ]);
        }

        //After the latest update time of the stored logistics information exceeds the configured expiration time, use the aggregate logistics query plug-in to query the shipment information and persist it into the database and cache.
        $interval_time = \Drupal::config('commerce_logistics.shipping_info_query')
          ->get('interval_time');
        if (time() - $shipment_info_newest_time > $interval_time) {
          $shipment_info->set('shipment_detail', $data['context']);
          $shipment_info->set('changed', $newest_query_time);
          $shipment_info->save();
          $state_result = $result['State'];
          $state = $shipment->getState()->getId();
          switch ($state_result) {
            case "2":
              $shipment_info->applyShipped($state, $shipment);
              break;
            case "3":
              $shipment_info->applyCompleted($state, $shipment);
              break;
            case "4":
              $shipment_info->applyIssue($state, $shipment);
              break;
            default:
              break;
          }
          \Drupal::cache('data')
            ->set($num, $data, time() + $interval_time);
        }
        /*============End========================================================================*/
      }
      return $data;
    }
    return NULL;
  }

  /**
   *  post data
   *
   * @param array $datas posted data
   *
   * @return false|string url response html
   */
  public function sendPost($datas) {
    $url = \Drupal::config('commerce_logistics.shipping_info_query')
      ->get('callback_url');
    $postdata = http_build_query($datas);
    $options = [
      'http' => [
        'method' => 'POST',
        'header' => 'Content-type:application/x-www-form-urlencoded',
        'content' => $postdata,
        'timeout' => 15 * 60, // Timeout (unit: s) 超时时间（单位:s）
      ],
    ];
    $context = stream_context_create($options);
    $result = file_get_contents($url, FALSE, $context);
    return $result;
  }

  /**
   * E-Commerce Sign Generator
   *
   * @param string $data
   * @param string $ApiKey
   *
   * @return string DataSign
   */
  public function encrypt($data, $ApiKey) {
    return urlencode(base64_encode(md5($data . $ApiKey)));
  }

}

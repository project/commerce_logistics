<?php

namespace Drupal\commerce_logistics\Plugin\Block;

use Drupal\commerce_logistics\LogisticsQueryPluginManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a logistics information block.
 * Used to display logistics information to customers
 * 用于将物流信息展示给顾客
 *
 * @Block(
 *   id = "shipment_info_block",
 *   admin_label = @Translation("Logistics information block"),
 *   category = @Translation("Commerce logistics")
 * )
 */
class ShipmentInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\commerce_logistics\LogisticsQueryPluginManager
   */
  protected $logisticsQueryManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The logistics detail storage.
   *
   * @var \Drupal\commerce_shipping\ShipmentStorage
   */
  protected $shipmentStorage;

  /**
   * The logistics detail storage.
   *
   * @var \Drupal\commerce_logistics\Entity\Handler\ShipmentInfoStorage
   */
  protected $shipmentInfoStorage;

  /**
   * The logistics company storage.
   *
   * @var \Drupal\commerce_shipping\ShippingMethodStorage
   */
  protected $shippingMethodStorage;

  /**
   * Constructs a new ShipmentInfoBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_logistics\LogisticsQueryPluginManager $logistics_query_manager
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array                       $configuration,
                                $plugin_id,
                                $plugin_definition,
    LogisticsQueryPluginManager $logistics_query_manager, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logisticsQueryManager = $logistics_query_manager;
    $this->routeMatch = $route_match;
    $this->shipmentStorage = $entity_type_manager->getStorage('commerce_shipment');
    $this->shipmentInfoStorage = $entity_type_manager->getStorage('shipment_info');
    $this->shippingMethodStorage = $entity_type_manager->getStorage('commerce_shipping_method');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.logistics_query'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'logistics_info_block',
      '#attached' => ['library' => ['commerce_logistics/base']],
      '#cache' => [
        'max-age' => 0,
      ],
    ];;
    $order = $this->routeMatch->getParameter('commerce_order');
    $data = [];
    if ($order instanceof OrderInterface) {
      $shipments = $this->shipmentStorage->loadMultipleByOrder($order);
      foreach ($shipments as $temp_shipment) {
        if (!empty($temp_shipment->getTrackingCode())) {
          $shipment = $temp_shipment;
        }
      }
      if (empty($shipment)) {
        $build['#data'] = NULL;
        return $build;
      }
      $mail_number = $shipment->getTrackingCode();
      $shipping_method_id = $shipment->getShippingMethod()->id();
      $config = \Drupal::config('commerce_logistics.shipping_info_query');
      $company_code = $config->get("code$shipping_method_id");
      if (empty($mail_number) || empty($company_code)) {
        $build['#data'] = NULL;
        return $build;
      }
      $plugin_id = \Drupal::service('commerce_logistics.default_logistics_query_resolver')
        ->resolve($shipment->getOrder());
      $nodes_logistics_query = $this->logisticsQueryManager->createInstance($plugin_id);
      $param = [
        'com' => $company_code,
        'num' => $mail_number,
      ];
      $data = $nodes_logistics_query->shipping_info_data($param);
    }
    $build['#data'] = $data;
    return $build;
  }

}

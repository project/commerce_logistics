<?php

namespace Drupal\commerce_logistics\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * TODO: class docs.
 *
 * @ViewsField("shipment_state_id")
 */
class ShipmentStateId extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
  }

  /**
   * Define the available options
   *
   * @return array
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    return $entity->getEntityTypeId();
  }

}

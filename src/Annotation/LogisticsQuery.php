<?php

namespace Drupal\commerce_logistics\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @see \Drupal\commerce_logistics\LogisticsQueryPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class LogisticsQuery extends Plugin {

  /**
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * @var string
   *
   * 0--Entered data;
   * 1--Logistics company aggregation platform(kdniao);
   * Company code--Open platform for single logistics company.
   * 0——录入的数据；
   * 1——物流聚合查询（快递鸟）；
   * 物流编码——单个物流公司的开放平台插件。
   *
   */
  public $type;

}

COMMERCE SHIPPING FEE
=====================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration

INTRODUCTION
------------

Provides logistics query functionality for Drupal Commerce.
The module uses the API of the express bird logistics query platform.

为Drupal Commerce提供物流查询功能。
该模块使用快递鸟物流查询平台的API。

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

按照您通常安装已贡献的Drupal模块的方式进行安装。
请参阅：https://www.drupal.org/documentation/install/modules-themes/modules-8
以获取更多信息。

CONFIGURATION
-------------

* Configure shipment info query
  Configure in "/admin/commerce/config/shipping/shipment-info-query".
* Configure Shipping methods
  Configure in "/admin/commerce/shipping-methods/manage/*".
  Select "Commerce logistics shipment" in "Shipment workflow".
* Enter the courier bird merchant interface access information in "KDNIAO PLATFORM SETTings" fieldset.
  Enter logistics company code.(Compiled by the National Post Office of China, provided by the official website of the National Post Office.)
* After the buyer user places an order, the merchant finds the corresponding order ("/admin/commerce/orders") in the order list,
  clicks "view", clicks "shipment", clicks "edit", and enters the corresponding "Tracking code".
  Without the "Tracking code" module, the logistics information of the order cannot be queried.
* After the input is completed, the buyer user can view their shipping information in the order interface.("/user/{uid}}/orders/{commerce_order}").
* There is a cache for logistics information, which avoids frequent calls to the query interface,
  so updates are not real-time. If human intervention is desired, the module provides an 'Interval time' at "/admin/commerce/config/shipping/shipment-info-query".
  After input, if the time from the latest query interface does not exceed the configured time here, the module will directly retrieve the cache.
  If it exceeds the configured time, the module will call the interface to query the latest logistics information.

* 配置发货信息查询
  在“/admin/commerce/config/shipping/shipping-info query”中进行配置。
* 配置运输方式
  在“/admin/commerce/shipping methods/manage/*”中进行配置。
  在“Shipment workflow”中选择“Commerce logistics shipment”。
* 在“快递鸟查询平台配置”字段集中输入"快递鸟商家接口接入"信息。
  输入物流公司代码。（由中国国家邮政局编制，国家邮政局官方网站提供。）
* 买家用户下单后，商家在订单列表中找到相应的订单（“/admin/comerce/orders”），
  “查看”->“发货”->“编辑”，然后输入相应的“跟踪码”(物流货运单号，由接入的物流公司提供)。
  如果没有“跟踪码”,模块则无法查询订单的物流信息。
* 输入完成后，买家用户可以在订单界面中查看他们的发货信息。（"/user/{uid}}/orders/{commerce_order}"）。
* 货运信息存在缓存，避免了频繁地调用查询接口，所以更新并非是实时的。
  如果想要人为的干预，模块提供了"时间间隔"(在"/admin/commerce/config/shipping/shipment-info-query")。
  输入后，如果距离最新一次查询接口的时间没有超过这里的配置时间，模块将直接取用缓存，如果超过了模块将调用接口查询最新物流信息。

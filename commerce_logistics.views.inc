<?php

function commerce_logistics_views_data_alter(array &$data) {
  $data['commerce_logistics']['shipment_state_id'] = [
    'title' => t('Show shipment state id'),
    'field' => [
      'title' => t('Show shipment state id'),
      'help' => t('The current shipment status will be displayed to the user in text'),
      'id' => 'shipment_state_value',
    ],
  ];
}
